import { Handler, NextFunction, Response, Request } from "express";
import BaseController from "../lib/BaseController.lib";
import {responseObject} from "../lib/helper.lib";

interface MarketInterface {
  list: Handler
}

class Market extends BaseController implements MarketInterface {
  constructor(baseUrl: string) {
    super(baseUrl);
  }

  list = async (req: Request, res: Response, next: NextFunction) => {
    res.json(responseObject([], true))
  }
}

export default Market
