
export function responseObject(data: any, status: boolean, error?: string | null) {
  return {
    data,
    status,
    error
  }
}
