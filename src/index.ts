import express, { Request, Response } from 'express';
import dotenv from "dotenv";
const app = express();

dotenv.config();

app.get('/', (_: Request, res: Response) => {
  res.send('Well done!');
})

app.listen(3000, () => {
  console.log('The application is listening on port 3000!');
})

process.once('SIGUSR2', function() {
  process.kill(process.pid, 'SIGUSR2');
});
